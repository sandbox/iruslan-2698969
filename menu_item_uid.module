<?php

/**
 * @file
 *
 * Allows to create menu links with dynamic uid of current user.
 */

/**
 * Collect all available items to be used as dynamic link.
 *
 * @return array
 */
function menu_item_uid_get_dynamic_items() {
  $items = &drupal_static(__FUNCTION__, NULL);

  if (is_null($items)) {
    $items = module_invoke_all('menu_item_uid_items');
  }

  return $items;
}

/**
 * Load function for %menu_item_uid_path_user placeholder.
 *
 * @param $uid
 * @param bool|FALSE $reset
 *
 * @return mixed
 */
function menu_item_uid_path_user_load($uid, $reset = FALSE) {
  return user_load($uid, $reset);
}

/**
 * To arg function for %menu_item_uid_path_user placeholder.
 *
 * @param $arg
 *
 * @return mixed
 */
function menu_item_uid_path_user_to_arg($arg) {
  return empty($arg) || $arg == '%menu_item_uid_path_user' ? $GLOBALS['user']->uid : $arg;
}

/**
 * Load function for %menu_item_uid_path_profile2_by_uid placeholder.
 *
 * @param $uid
 * @param $type_name
 *
 * @return array|bool|mixed|\Profile
 */
function menu_item_uid_path_profile2_by_uid_load($uid, $type_name) {
  return profile2_by_uid_load($uid, $type_name);
}

/**
 * To arg function for %menu_item_uid_path_profile2_by_uid placeholder.
 *
 * @param $arg
 *
 * @return mixed
 */
function menu_item_uid_path_profile2_by_uid_to_arg($arg) {
  return empty($arg) || $arg == '%menu_item_uid_path_profile2_by_uid' ? $GLOBALS['user']->uid : $arg;
}

/**
 * Implements hook_menu_alter().
 *
 * @param $items
 */
function menu_item_uid_menu_alter(&$items) {
  // We need to copy all original router items to have proper load an to_args funcs.
  $dynamic_items = menu_item_uid_get_dynamic_items();
  foreach ($dynamic_items as $path => $original_part) {
    if (isset($items[$path])) {
      $dynamic_part = '%menu_item_uid_path_' . str_replace('%', '', $original_part);
      $dynamic_path = str_replace($original_part, $dynamic_part, $path);
      $items[$dynamic_path] = $items[$path];
    }
  }
}

/**
 * Implements hook_translated_menu_link_alter().
 *
 * Because we store link paths with placeholder,
 * here we need to insret real dynamic value during render.
 *
 * @param $item
 * @param $map
 */
function menu_item_uid_translated_menu_link_alter(&$item, $map) {
  // Don't correct items on menu link edit page and menu manage page.
  if (strpos($_GET['q'], 'admin/structure/menu/item/') === FALSE && strpos($_GET['q'], 'admin/structure/menu/manage/') === FALSE) {
    $dynamic_items = menu_item_uid_get_dynamic_items();
    foreach ($dynamic_items as $path => $original_part) {
      $dynamic_part = '%menu_item_uid_path_' . str_replace('%', '', $original_part);
      if (strpos($item['link_path'], $dynamic_part) !== FALSE) {
        $item['link_path'] = str_replace($dynamic_part, $GLOBALS['user']->uid, $item['link_path']);
      }
    }
  }
}

/**
 * Implements hook_FORM_ID_alter().
 *
 * @param $form
 * @param $form_state
 */
function menu_item_uid_form_menu_edit_item_alter(&$form, $form_state) {
  if (($pos = array_search('menu_edit_item_validate', $form['#validate'])) !== FALSE) {
    $form['#validate'][$pos] = 'menu_item_uid_menu_edit_item_validate';
  }
}

/**
 * Fork of menu_edit_item_validate().
 * We need it to skip validation for links with defined dynamic parts.
 *
 * @param $form
 * @param $form_state
 */
function menu_item_uid_menu_edit_item_validate($form, &$form_state) {
  $item = &$form_state['values'];
  $normal_path = drupal_get_normal_path($item['link_path']);
  if ($item['link_path'] != $normal_path) {
    drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $item['link_path'], '%normal_path' => $normal_path)));
    $item['link_path'] = $normal_path;
  }
  if (!url_is_external($item['link_path'])) {
    $parsed_link = parse_url($item['link_path']);
    if (isset($parsed_link['query'])) {
      $item['options']['query'] = drupal_get_query_array($parsed_link['query']);
    }
    else {
      // Use unset() rather than setting to empty string
      // to avoid redundant serialized data being stored.
      unset($item['options']['query']);
    }
    if (isset($parsed_link['fragment'])) {
      $item['options']['fragment'] = $parsed_link['fragment'];
    }
    else {
      unset($item['options']['fragment']);
    }
    if (isset($parsed_link['path']) && $item['link_path'] != $parsed_link['path']) {
      $item['link_path'] = $parsed_link['path'];
    }
  }

  // If inserted item contains at least one dynamic part, skip validation.
  $validate_path = TRUE;
  $dynamic_items = menu_item_uid_get_dynamic_items();
  foreach ($dynamic_items as $path => $original_part) {
    $dynamic_part = '%menu_item_uid_path_' . str_replace('%', '', $original_part);
    if (strpos($item['link_path'], $dynamic_part) !== FALSE) {
      $validate_path = FALSE;
      break;
    }
  }
  if ($validate_path) {
    if (!trim($item['link_path']) || !drupal_valid_path($item['link_path'], TRUE)) {
      form_set_error('link_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $item['link_path'])));
    }
  }
}
